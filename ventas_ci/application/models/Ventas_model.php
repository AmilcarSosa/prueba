<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ventas_model extends CI_Model {

	public function getproductos($dato){
		$this->db->select("id,nombre as label,precio,cantidad");
		$this->db-from("productos");
		$this->db->like("nombre",$dato);
		$resultado = $this->db->get();
		return $resultado->result_array();
	}
	public function getventas(){
		$resultados = $this->db->get("venta");
		return $resultados->result();
	}

}