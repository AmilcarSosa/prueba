<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios_model extends CI_Model {

	public function login($usuario, $password){
		$this->db->where("usuario", $usuario);
		$this->db->where("password", $password);

		$resultados = $this->db->get("usuario");
		if ($resultados->num_rows() > 0) {
			return $resultados->row();
		}
		else{
			return false;
		}
	}

    public function getdatos(){
		$this->db->select("p.*");
		$this->db->from("usuario p");
		$resultados = $this->db->get();
		return $resultados->result();
	}
	public function getusuario($id){
		$this->db->where("id",$id);
		$resultado = $this->db->get("usuario");
		return $resultado->row();
	}
	public function save($data){
		return $this->db->insert("usuario",$data);
	}

	public function update($id,$data){
		$this->db->where("id",$id);
		return $this->db->update("usuario",$data);
	}

	public function delete($id){
		$this->db->where('id',$id);
		$this->db->delete('usuario');
	}
}
