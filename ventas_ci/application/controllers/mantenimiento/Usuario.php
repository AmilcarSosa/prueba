<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model("usuarios_model");
	}

	public function index()
	{
		$data  = array(
			'usuario' => $this->Usuarios_model->getusuarios(), 
		);
		$this->load->view("layouts/header");
		$this->load->view("layouts/aside");
		$this->load->view("admin/usuario/list",$data);
		$this->load->view("layouts/footer");

	}
	public function add(){
		$this->load->view("layouts/header");
		$this->load->view("layouts/aside");
		$this->load->view("admin/productos/add");
		$this->load->view("layouts/footer");
	}

	public function store(){
		$nombre = $this->input->post("nombre");
		$apellidos = $this->input->post("precio");
		$usuario = $this->input->post("usuario");
		$password = $this->input->post("password");

		$data  = array(
			'nombre' => $nombre,
			'apellidos' => $apellidos,
			'usuario' => $usuario,
			'password' => $password,
		);

		if ($this->Productos_model->save($data)) {
			redirect(base_url()."mantenimiento/usuario");
		}
		else{
			$this->session->set_flashdata("error","No se pudo guardar la informacion");
			redirect(base_url()."mantenimiento/usrio/add");
		}
	}

	public function edit($id){
		$data =array( 
			"producto" => $this->usuarios_model->getdatos($id),
		);
		$this->load->view("layouts/header");
		$this->load->view("layouts/aside");
		$this->load->view("admin/productos/edit",$data);
		$this->load->view("layouts/footer");
	}

	public function update(){
		$idusuario=$this->input->post("id");
		$nombre = $this->input->post("nombre");
		$apellidos = $this->input->post("aperllidos");
		$usuario = $this->input->post("usuario");
		$password = $this->input->post("categoria");
		$data  = array(
			'nombre' => $nombre,
			'apellidos' => $apellidos,
			'usuario' => $usuario,
			'password' => $password,
		);
		if ($this->Productos_model->update($idproducto,$data)) {
			redirect(base_url()."mantenimiento/productos");
		}
		else{
			$this->session->set_flashdata("error","No se pudo guardar la informacion");
			redirect(base_url()."mantenimiento/productos/edit/".$idproducto);
		}
	}
	public function delete($id){
		$this->usuario_model->delete($id);
		echo "mantenimiento/usuario";
	}

}