<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ventaslist extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model("ventas_model");
	}

	
	public function index()
	{
		$data  = array(
			'venta' => $this->ventas_model->getventas(), 
		);
		$this->load->view("layouts/header");
		$this->load->view("layouts/aside");
		$this->load->view("admin/ventas/list",$data);
		$this->load->view("layouts/footer");

	}
}