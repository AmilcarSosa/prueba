<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ventas extends CI_Controller {

	public function __construct(){
		parent::__construct();

	}

	public function index(){
		$this->load->view("layouts/header");
		$this->load->view("layouts/aside");
		$this->load->view("admin/ventas/add");
		$this->load->view("layouts/footer");

	}

	public function getproductos(){
		$valor = $this->input->post("valor")
		$datos=$this->Ventas_model->getproductos($valor);
		echo json_encode($datos);

	}
	public function add(){
		$this->load->view("layouts/header");
		$this->load->view("layouts/aside");
		$this->load->view("admin/ventas/add");
		$this->load->view("layouts/footer");


		$fecha= $this->input->post("nombre");
		$total = $this->input->post("precio");
		$usuario_id = $this->input->post("cantidad");

		$data  = array(
			'fecha' => $fecha,
			'total' => $total,
			'usuario_id' => $usuaario_id,
		);

		if ($this->Productos_model->save($data)) {
			redirect(base_url()."mantenimiento/productos");
		}
		else{
			$this->session->set_flashdata("error","No se pudo guardar la informacion");
			redirect(base_url()."mantenimiento/productos/add");
		}
	}

}

